<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    xmlns:exsl="http://exslt.org/common"
    xmlns:h="HouseChema"
    xmlns:i="HouseInfo"
    xmlns:r="RoomsChema"
    version="1.0"
    exclude-result-prefixes="exsl str"
    extension-element-prefixes="exsl str">
    
    <xsl:variable name="lowCase">йцукенгшщзхъфывапролджэячсмитьбюqwertyuiopasdfghjklzxcvbnm</xsl:variable>
    <xsl:variable name="upCase">ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮQWERTYUIOPASDFGHJKLZXCVBNM</xsl:variable>
    
    
    
    <xsl:variable name="city2">
        <xsl:for-each select="//h:House[2]/@City">
            <foo>
                <xsl:value-of select="."/>
            </foo>
        </xsl:for-each>
    </xsl:variable>  
    <xsl:variable name="adr2">
        <xsl:for-each select="//h:House[2]//i:Address">
            <foa>
                <xsl:value-of select="."/>
            </foa>
        </xsl:for-each>
    </xsl:variable> 
    <xsl:variable name="block2">
        <xsl:for-each select="//h:House[2]//@number">
            <fos>
                <xsl:value-of select="."/>
            </fos>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="guests2">
        <xsl:value-of select="sum(exsl:node-set(//h:House[2]//@guests))"/>
    </xsl:variable>
    <xsl:variable name="roomsHouse2">
        <xsl:value-of select="count(//h:House[2]//r:Room)"/>
    </xsl:variable>
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <AllRooms> 
        <xsl:apply-templates select="//h:House[1]/*">
            <xsl:sort select="//h:House[1]/@City"/>
            <xsl:sort select="//h:House[1]//@number"/>
            <xsl:sort select="//h:House[1]//@nuber"/>
        </xsl:apply-templates> 
        <xsl:apply-templates select="//h:House[2]/*">
            <xsl:sort select="//h:House[2]/@City"/>
            <xsl:sort select="//h:House[2]//@number"/>
            <xsl:sort select="//h:House[2]//@nuber"/>
        </xsl:apply-templates>  
        </AllRooms>
   </xsl:template>
        
    
    <xsl:template name="for" match="//h:House[1]/*"> 
        <xsl:param name="i" select="1"/>
        <xsl:param name="k" select="1"/> 
        <xsl:param name="n" select="count(exsl:node-set(//h:House[1]//r:Room/@nuber))+1"/>       
        <xsl:variable name="city1">
            <xsl:for-each select="//h:House[1]//@City">
                <foo>
                    <xsl:value-of select="."/>
                </foo>
            </xsl:for-each>
        </xsl:variable>  
        <xsl:variable name="adr1">
            <xsl:for-each select="//h:House[1]//i:Address">
                <foa>
                    <xsl:value-of select="."/>
                </foa>
            </xsl:for-each>
        </xsl:variable> 
        <xsl:variable name="block1">
            <xsl:for-each select="//h:House[1]//@number">
                <fos>
                    <xsl:value-of select="."/>
                </fos>                             
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="guests1">
            <xsl:value-of select="sum(exsl:node-set(//h:House[1]//@guests))"/>
        </xsl:variable>
        <xsl:variable name="roomsHouse1">
            <xsl:value-of select="count(//h:House[1]//r:Room)"/>
        </xsl:variable>  
        <xsl:if test="$i &lt; $n"> 
            <xsl:variable name="nuber1">
                <xsl:for-each select="//h:House[1]//@nuber">
                    <fod>
                        <xsl:value-of select="."/>
                    </fod>
                </xsl:for-each>
            </xsl:variable>
            <Room> 
                <Adress>  
                    <xsl:value-of select="concat(translate(exsl:node-set($city1)/foo[1],$lowCase,$upCase),'/',exsl:node-set($adr1)/foa[1],'/',exsl:node-set($block1)/fos[$k],'/',exsl:node-set($nuber1)/fod[$i])"/>   
                </Adress>
                <HouseRoomsCount>
                    <xsl:value-of select="$roomsHouse1"/>
                </HouseRoomsCount> 
                <BlockRoomsCount>         </BlockRoomsCount>
                <HouseGuestsCount>                  
                    <xsl:value-of select="$guests1"/>
                </HouseGuestsCount>
                <GuestsPorRoomAverage>
                    <xsl:variable name="aver">
                        <xsl:value-of select="$roomsHouse1 div $guests1"/>
                    </xsl:variable>
                    <xsl:value-of select="ceiling(number(exsl:node-set($aver)))"/>
                </GuestsPorRoomAverage>
                <xsl:variable name="g">
                    <xsl:for-each select="//h:House[1]//@guests">
                        <fof>
                            <xsl:value-of select="."/>
                        </fof>
                    </xsl:for-each>                        
                    </xsl:variable>
                <Allocated>
                    <xsl:attribute name="Single">
                    <xsl:choose>
                        <xsl:when test="exsl:node-set($g)/fof[$i] = '1'">
                            <xsl:text>true</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>false</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Double">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '2'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Triple">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '3'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Quarter">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '4'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>           
                </Allocated>
            </Room>  
            
                <xsl:call-template name="for">
                    <xsl:with-param name="i" select="$i + 1"/>
                    <xsl:with-param name="n" select="$n"/>
                    <xsl:with-param name="k" select="$k"/>
                </xsl:call-template>
        </xsl:if>        
    </xsl:template>
        
    <xsl:template name="for2" match="//h:House[2]/*">
        <xsl:param name="i" select="1"/>
        <xsl:param name="k" select="1"/> 
        <xsl:param name="n" select="count(exsl:node-set(//h:House[2]//r:Room/@nuber))+1"/>       
        <xsl:variable name="city2">
            <xsl:for-each select="//h:House[2]/@City">
                <foo>
                    <xsl:value-of select="."/>
                </foo>
            </xsl:for-each>
        </xsl:variable>  
        <xsl:variable name="adr2">
            <xsl:for-each select="//h:House[2]//i:Address">
                <foa>
                    <xsl:value-of select="."/>
                </foa>
            </xsl:for-each>
        </xsl:variable> 
        <xsl:variable name="block2">
            <xsl:for-each select="//h:House[2]//@number">
                <fos>
                    <xsl:value-of select="."/>
                </fos>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="guests2">
            <xsl:value-of select="sum(exsl:node-set(//h:House[2]//@guests))"/>
        </xsl:variable>
        <xsl:variable name="roomsHouse2">
            <xsl:value-of select="count(//h:House[2]//r:Room)"/>
        </xsl:variable>
        
        <xsl:if test="$i &lt; $n"> 
            <xsl:variable name="nuber2">
                <xsl:for-each select="//h:House[2]//@nuber">
                    <fod>
                        <xsl:value-of select="."/>
                    </fod>
                </xsl:for-each>
            </xsl:variable>
            <Room> 
                <Adress>  
                    <xsl:value-of select="concat(translate(exsl:node-set($city2)/foo[1],$lowCase,$upCase),'/',exsl:node-set($adr2)/foa[1],'/',exsl:node-set($block2)/fos[$k],'/',exsl:node-set($nuber2)/fod[$i])"/>   
                </Adress>
                <HouseRoomsCount>
                    <xsl:value-of select="$roomsHouse2"/>
                </HouseRoomsCount> 
                <BlockRoomsCount> </BlockRoomsCount>
                <HouseGuestsCount>                  
                    <xsl:value-of select="$guests2"/>
                </HouseGuestsCount>
                <GuestsPorRoomAverage>
                    <xsl:variable name="aver">
                        <xsl:value-of select="$roomsHouse2 div $guests2"/>
                    </xsl:variable>
                    <xsl:value-of select="ceiling(number(exsl:node-set($aver)))"/>
                </GuestsPorRoomAverage>
                <xsl:variable name="g">
                    <xsl:for-each select="//h:House[2]//@guests">
                        <fof>
                            <xsl:value-of select="."/>
                        </fof>
                    </xsl:for-each>                    
                </xsl:variable>
                <Allocated>
                    <xsl:attribute name="Single">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '1'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Double">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '2'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Triple">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '3'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="Quarter">
                        <xsl:choose>
                            <xsl:when test="exsl:node-set($g)/fof[$i] = '4'">
                                <xsl:text>true</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>false</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>           
                </Allocated>
            </Room>       
            <xsl:call-template name="for2">
                <xsl:with-param name="i" select="$i + 1"/>
                <xsl:with-param name="n" select="$n"/>
                <xsl:with-param name="k" select="$k"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>