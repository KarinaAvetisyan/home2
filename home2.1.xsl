<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:str="http://exslt.org/strings"
    xmlns:exsl="http://exslt.org/common"
    exclude-result-prefixes="exsl str"
    extension-element-prefixes="exsl str">
    <xsl:template match="/">
        <xsl:param name="str"/>
        <list>
            <xsl:for-each select="Guests">   
                <xsl:variable name="ty">
                    <xsl:for-each select="//Guest/Type">
                        <foo>
                            <xsl:value-of select="."/>
                        </foo>  
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="age">
                    <xsl:for-each select="//Guest/@Age">
                        <fot>
                            <xsl:value-of select="."/>
                        </fot>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="notion">
                    <xsl:for-each select="//Guest/@Nationalty">
                        <fos>
                            <xsl:value-of select="."/>
                        </fos>  
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="gender">
                    <xsl:for-each select="//Guest/@Gender">
                        <fod>
                            <xsl:value-of select="."/>
                        </fod>  
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="name">
                    <xsl:for-each select="//Guest/@Name">
                        <fof>
                            <xsl:value-of select="."/>
                        </fof>  
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="address">
                    <xsl:for-each select="//Guest//Address">
                        <fog>
                            <xsl:value-of select="."/>
                        </fog>  
                    </xsl:for-each>
                </xsl:variable>
                
                <guest>
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[1],'/',exsl:node-set($age)/fot[1],'/',exsl:node-set($notion)/fos[1],'/',exsl:node-set($gender)/fod[1],'/',exsl:node-set($name)/fof[1],'/',exsl:node-set($address)/fog[1],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[2],'/',exsl:node-set($age)/fot[2],'/',exsl:node-set($notion)/fos[2],'/',exsl:node-set($gender)/fod[2],'/',exsl:node-set($name)/fof[2],'/',exsl:node-set($address)/fog[2],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[3],'/',exsl:node-set($age)/fot[3],'/',exsl:node-set($notion)/fos[3],'/',exsl:node-set($gender)/fod[3],'/',exsl:node-set($name)/fof[3],'/',exsl:node-set($address)/fog[3],'|')"/>
               
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[4],'/',exsl:node-set($age)/fot[4],'/',exsl:node-set($notion)/fos[4],'/',exsl:node-set($gender)/fod[4],'/',exsl:node-set($name)/fof[4],'/',exsl:node-set($address)/fog[4],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[5],'/',exsl:node-set($age)/fot[5],'/',exsl:node-set($notion)/fos[5],'/',exsl:node-set($gender)/fod[5],'/',exsl:node-set($name)/fof[5],'/',exsl:node-set($address)/fog[5],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[6],'/',exsl:node-set($age)/fot[6],'/',exsl:node-set($notion)/fos[6],'/',exsl:node-set($gender)/fod[6],'/',exsl:node-set($name)/fof[6],'/',exsl:node-set($address)/fog[6],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[7],'/',exsl:node-set($age)/fot[7],'/',exsl:node-set($notion)/fos[7],'/',exsl:node-set($gender)/fod[7],'/',exsl:node-set($name)/fof[7],'/',exsl:node-set($address)/fog[7],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[8],'/',exsl:node-set($age)/fot[8],'/',exsl:node-set($notion)/fos[8],'/',exsl:node-set($gender)/fod[8],'/',exsl:node-set($name)/fof[8],'/',exsl:node-set($address)/fog[8],'|')"/>
                
                    <xsl:value-of select="concat($str,exsl:node-set($ty)/foo[9],'/',exsl:node-set($age)/fot[9],'/',exsl:node-set($notion)/fos[9],'/',exsl:node-set($gender)/fod[9],'/',exsl:node-set($name)/fof[9],'/',exsl:node-set($address)/fog[9],'|')"/>
                </guest> 
            </xsl:for-each>
        </list>     
    </xsl:template>
</xsl:stylesheet>