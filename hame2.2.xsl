<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    xmlns:exsl="http://exslt.org/common"
    version="1.0"
    exclude-result-prefixes="exsl str"
    extension-element-prefixes="exsl str">    
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <Guests> 
            <xsl:for-each select="list/guest">
                <xsl:variable name="goests"> 
                    <xsl:for-each select="str:tokenize(.,'|')">
                        <foo>
                            <xsl:value-of select="."/>
                        </foo>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:for-each select="exsl:node-set($goests)/foo">
                    <Guest>
                        <xsl:variable name="guest"> 
                            <xsl:for-each select="str:tokenize(.,'/')">
                                <fot>
                                    <xsl:value-of select="."/>
                                </fot>
                            </xsl:for-each> 
                        </xsl:variable>  
                        <xsl:attribute name="Age">
                            <xsl:value-of select="exsl:node-set($guest)/fot[2]"/>
                        </xsl:attribute>
                        <xsl:attribute name="Nationalty"> 
                            <xsl:value-of select="exsl:node-set($guest)/fot[3]"/>
                        </xsl:attribute>
                        <xsl:attribute name="Gender"> 
                            <xsl:value-of select="exsl:node-set($guest)/fot[4]"/>
                        </xsl:attribute>
                        <xsl:attribute name="Name"> 
                            <xsl:value-of select="exsl:node-set($guest)/fot[5]"/>
                        </xsl:attribute> 
                        <xsl:element name="Type">
                            <xsl:value-of select="exsl:node-set($guest)/fot[1]"/>
                        </xsl:element>
                        <Profile>
                            <xsl:element name="Address">
                                <xsl:value-of select="exsl:node-set($guest)/fot[6]"/>
                            </xsl:element>
                        </Profile> 
                    </Guest>
                </xsl:for-each>
            </xsl:for-each>
        </Guests>
    </xsl:template>
</xsl:stylesheet>